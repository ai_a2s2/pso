Monochromatic Triangle

In an undirected graph G (V , E) with the set of vertices V and the set of edges E
find a partitioning of two disjoint subsets E1 and E2 so that each of the two sub-graphs
G (V , E ) and does not contain a triangle - any three different nodes , , 1 1 G (V , E ) 2 2 u v w
from V are not connected with edges from the same sub-graph ((u, v) , (u,w) and (v,w)
are not in E ).


For the initial graph G with V = {1, 2, 3, 4, 5} and E = {(1, 2), (1, 3), (1, 4), (1, 5),(2, 3),(3, 4),
(3, 5), (4, 5)} a solution can be :
E {(1, ), (1, 4), (3, ), (4, )} and . 1 = 2 5 5 E {(1, ), (1, ), (2.3), (3, )} 